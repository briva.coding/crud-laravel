@extends ('adminLte.master')

@section('title')
	Edit Cast id {{$cast->id}}
@endsection

@section ('content')
	<form action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="nama">Nama Cast</label>
                <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="nama" placeholder="Masukkan Nama Cast">
                <label for="umur">Umur</label>
                <input type="text" class="form-control mb-1" name="umur" value="{{$cast->umur}}" id="umur" placeholder="Masukkan Umur">
                <label for="bio">Bio</label>
                <input type="text" class="form-control" name="bio" value="{{$cast->bio}}" id="bio" placeholder="Masukkan Bio">
                @error('nama')
                    
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                    	{{ $message }}
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    <span aria-hidden="true">&times;</span>
					  </button>
					</div>
                @enderror
            </div>
            
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
@endsection