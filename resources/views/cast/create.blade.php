@extends ('adminLte.master')

@section('link1')
	Cast
@endsection

@section('link2')
	Create New Cast
@endsection


@section ('content')
	<form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label for="nama">Nama Cast</label>
                <input type="text" class="form-control mb-1" name="nama" id="nama" placeholder="Masukkan Nama Cast">
                <label for="umur">Umur</label>
                <input type="text" class="form-control mb-1" name="umur" id="umur" placeholder="Masukkan Umur">
                <label for="bio">Bio</label>
                <input type="text" class="form-control" name="bio" id="bio" placeholder="Masukkan Bio">
                <!-- @error('nama')
                    
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                      {{ $message }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @enderror -->
            </div>
            
            <button type="submit" class="btn btn-primary">Tambah</button>
            
        </form>
@endsection