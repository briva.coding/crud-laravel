@extends ('adminLte.master')

@section('title')
	Edit Genre id {{$genre->id}}
@endsection

@section ('content')
	<form action="/genre/{{$genre->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="nama">Nama Genre</label>
                <input type="text" class="form-control" name="nama" value="{{$genre->nama}}" id="nama" placeholder="Masukkan Nama Genre">
                @error('nama')
                    
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                    	{{ $message }}
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    <span aria-hidden="true">&times;</span>
					  </button>
					</div>
                @enderror
            </div>
            
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
@endsection