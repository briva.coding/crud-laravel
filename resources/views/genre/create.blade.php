@extends ('adminLte.master')

@section('title')
	Tambah Genre
@endsection

@section ('content')
	<form action="/genre" method="POST">
            @csrf
            <div class="form-group">
                <label for="nama">Nama Genre</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama Genre">
                @error('nama')
                    
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                    	{{ $message }}
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    <span aria-hidden="true">&times;</span>
					  </button>
					</div>
                @enderror
            </div>
            
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
@endsection